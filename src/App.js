import React, { Component } from 'react';
import logo from './logo.svg';
import './App.css';

class App extends Component {
  constructor(props) {
    super(props);
    this.state = {
      financialGoal: ''
    }
  }
  _fizzBuzz(num) {
    if (num % 3 && num % 5 === 0) {
      this.setState({fizzBuzz: 'FizzBuzz'});
    } else if (num % 5 === 0) {
      this.setState({fizzBuzz: 'Buzz'});
    } else if (num % 3 === 0) {
      this.setState({fizzBuzz: 'Fizz'});
    } else {
      this.setState({fizzBuzz: ''});
    }
  }
  
 handleChange(evt) { 
    const financialGoal = (evt.target.validity.valid) ? evt.target.value : this.state.financialGoal;
    this.setState({ financialGoal });
    this._fizzBuzz(this.state.financialGoal);
  }
  render() {
    return (
      <div className="col-sm-12 text-center">
        <h2>
          {this.state.fizzBuzz || this.state.financialGoal}
        </h2>
        <div className="btn-group">
          <input type="text" pattern="[0-9]*" onInput={this.handleChange.bind(this)} value={this.state.financialGoal} />
        </div>
      </div>
    );
  }
}

export default App;
